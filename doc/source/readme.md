Hi,

this site is an **experiment** to allow people to play [Jocly](https://github.com/mi-g/jocly) games.

**Jocly** is an open source library of board games, more than a hundred are available, but the game server has been down for quite a while now.

This experiment **does not pretend** to have the efficiency and all the features that were present on jocly.com, nor is it as secure, but :

- you should be able to  play
- you should be able to install this small game server anywhere

## Pros and cons

You choose what is pro or con :

- no database, just need a web server with PHP. Games are stored in simple readable text files (json)
- no login, no password (may change), absolutely nothing personal is stored, only game turns
- no adverts, no tracking

- very basic, it just lets you :
  - play alone vs computer
  - create a match, you will have to send a link to your opponent
- works with old school polling
- you can cheat : anyone with the proper link can play A or B. But we are gentlemen, aren't we?

## Disclaimer

So this is an experiment. The site can be modified, removed or games storage reset at anytime. No guarantee at all :)

## Contact

Jocly was created by Michel Gutierrez : https://github.com/mi-g

My name is Jerome Choain, aka [@jcfrog](https://mamot.fr/@jcfrog). I worked for Michel on Jocly for 4 years. Since then I have continued to create games and improvments for clients. 

You can email me at jerome at choain dot fr

